const assert = require("assert");

const lib = require('../index.js');

describe("Connect function", () =>
{
  it("Should not fail", (done) =>
  {
    lib
    .connect({host: "127.0.0.1", port: "6379"})
    .then(() =>
    {
      console.log('OK');
      done();
    })
    .catch(err =>
    {
      console.log(err);
      done();
    });
  });

  it("Test client return", (done) =>
  {
    const client = lib.getClient();

    client.set("test", "value");

    client.get('test', (err, result) =>
    {
      assert.equal(err, null);
      assert.equal(result, "value");
      done();
    })
  })
});





