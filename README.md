# RedisHandler

This module handles a simple connection to Redis DB server.

## Installation
<code>$ npm install redis-handler-nt --save</code>

## Usage

- <code>connect(config)</code>

Example of config

````json
  {
    "host": "127.0.0.1",
    "port": "6379"
  }
````

See the allowed properties on the official documentation [https://www.npmjs.com/package/redis#rediscreateclient].

- <code>getClient()</code>

Get the client redis object.
 
Then, you just need to call this function in the files that will use it.

